# word-book-fx

#### 介绍
用于查单词, 背单词的javaFX+SpringBoot+MP(SQLite)的客户端程序.

#### 软件架构

**前台**: JavaFX + JavaFX Scene Builder 2.0 

**后台**: SpringBoot 2.X + Mybatis-plus + Lombok 

**数据库**: SQLite 

#### 安装教程

**非开发者运行:**初次运行需要安装java运行环境,点击 install.bat,会自动安装运行环境, 完毕后点击 wordbook.bat 即可运行. 

1. ![image-20230427152550034](/assets/image-20230427152550034.png)

   

2. ![image-20230427152611527](/assets/image-20230427152611527.png)

3.  以管理员模式进入终端 -> java -jar wordbook.jar

#### 使用说明

1.  Local DB loaded in "C:\" when application have launched.
2.  If you want to read run-time log you can press "Win + R" and enter "CMD" to open Command Prompt Interface. Then find your "wordbook.jar" to input "java -jar wordbook.jar" to view the log in the Command Prompt Interface.

#### preview

##### 主页面

![image-20230427153300619](/assets/image-20230427153300619.png)



中英文查单词与添加单词

![image-20230427153350869](/assets/image-20230427153350869.png)

![image-20230427153420156](/assets/image-20230427153420156.png)

![image-20230427153513169](/assets/image-20230427153513169.png)

未有此词

![image-20230427153801747](/assets/image-20230427153801747.png)

未有此意

![image-20230427153821637](/assets/image-20230427153821637.png)

重复添加

![image-20230427153527099](/assets/image-20230427153527099.png)

统计总数

![image-20230427153619182](/assets/image-20230427153619182.png)

​	

##### 随机单词

appearance

![image-20230427153909542](/assets/image-20230427153909542.png)

点击shuffle进行打乱背单词

![image-20230427154000240](/assets/image-20230427154000240.png)

识意

![image-20230427154016914](/assets/image-20230427154016914.png)

动态进度

![image-20230427154037369](/assets/image-20230427154037369.png)

"悔棋"

![image-20230427154055599](/assets/image-20230427154055599.png)

##### 修正单词

![image-20230427154153413](/assets/image-20230427154153413.png)

双击修改 回车保存并刷新

![image-20230427154209538](/assets/image-20230427154209538.png)

#### 翻译功能
![img.png](/assets/img.png)


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
