package com.aryankh.wordbookfx;

import com.aryankh.wordbookfx.view.WordBookMainView;
import de.felixroske.jfxsupport.AbstractJavaFxApplicationSupport;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author AryanKH
 */
@SpringBootApplication
@MapperScan("com.aryankh.wordbookfx.dao")
public class WordBookFxApplication extends AbstractJavaFxApplicationSupport {

	public static void main(String[] args) {
		launch(WordBookFxApplication.class, WordBookMainView.class,args);
		
	}
}
