package com.aryankh.wordbookfx.constant;

/**
 * @Author AryanKH
 * @Date 2023/6/6 19:05
 */
public class MouseClickConstants {
    public static final int DOUBLE_CLICK = 2;

}
