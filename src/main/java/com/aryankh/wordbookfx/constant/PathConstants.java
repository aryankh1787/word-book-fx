package com.aryankh.wordbookfx.constant;

/**
 * @Author AryanKH
 * @Date 2023/4/24 16:13
 */
public class PathConstants {
    public static final String C_PATH = "C:/";
    public static final String DB_NAME = "wordbook.sqlite";
    public static final String THEME_NAME = "themes.tme";
    public static final String C_DB_NAME = "C:/wordbook.sqlite";
    public static final String C_THEME_NAME = "C:/themes.tme";
    public static final String CLASS_PATH_RESOURCE_PATH = "/wordbook.sqlite";
    public static final String ACCESS_KEY_PATH = "C:/accessKey.ak";
    public static final String TABLE_NAME_PATH = "C:/tableName.tn";
    public static final String THEME_NAME_PATH = "C:/themes.tme";

    public static final String AUDIO_PATH = "C:/audio.mp3";


}
