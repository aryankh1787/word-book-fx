package com.aryankh.wordbookfx.constant;

/**
 * @Author AryanKH
 * @Date 2023/5/30 9:47
 */
public class SqliteConstants {
    public static final String SQLITE_SEQUENCE = "sqlite_sequence";
}
