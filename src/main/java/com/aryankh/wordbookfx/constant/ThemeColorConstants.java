package com.aryankh.wordbookfx.constant;

/**
 * @Author AryanKH
 * @Date 2023/6/16 15:52
 */
public class ThemeColorConstants {
    public static final String PURPLISH_BLACK = "#282A36";
    public static final String MODENA_PURPLE = "#414450";
    public static final String GRAY_WHITE = "#CED0D6";
    public static final String WHITE = "#FFFFFF";
    public static final String BLACK = "#000000";
    public static final String DARK_BLUE = "#5182E4";
    public static final String LIGHT_GRAY = "#F2F2F2";

    public static final String DEEPLY_WHITE = "#D1D1D1";

    public static final String LITE_BLACK = "#C7C7C7";

}
