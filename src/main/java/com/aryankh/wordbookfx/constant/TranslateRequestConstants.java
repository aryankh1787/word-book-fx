package com.aryankh.wordbookfx.constant;

/**
 * @Author AryanKH
 * @Date 2023/4/28 9:26
 */
public class TranslateRequestConstants {
    public static final String FORMAT_TYPE = "text";
    public static final String CHINESE = "zh";
    public static final String ENGLISH = "en";
    public static final String SCENE = "general";

    public static final String TEST_ACCESS_ID = "LTAI5tNtxoPPZ3KDLSBQAjpK";
    public static final String TEST_ACCESS_SECRET = "JKQSaurfzyWMzpDunkwGpJe17zZDRH";

    public static final String YD_TEST_ACCESS_ID = "6aa55f3644d15483";
    public static final String YD_TEST_ACCESS_SECRET = "0W4CPyOHji4IWK8lunjji704LXBE6MjS";
}
