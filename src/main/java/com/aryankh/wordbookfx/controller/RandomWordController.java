package com.aryankh.wordbookfx.controller;

import cn.hutool.core.io.file.FileReader;
import cn.hutool.extra.spring.SpringUtil;

import com.aryankh.wordbookfx.WordBookFxApplication;
import com.aryankh.wordbookfx.constant.PathConstants;
import com.aryankh.wordbookfx.constant.ThemeColorConstants;
import com.aryankh.wordbookfx.entity.ReloadWordEntity;
import com.aryankh.wordbookfx.service.WordService;
import com.aryankh.wordbookfx.util.ConvertProgressUtil;
import com.aryankh.wordbookfx.util.LocalFileUtils;
import com.aryankh.wordbookfx.view.WordBookMainView;
import de.felixroske.jfxsupport.FXMLController;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ToolBar;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.paint.Paint;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.net.URL;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;

/**
 * @Author AryanKH
 * @Date 2023/4/25 19:49
 */
@FXMLController
public class RandomWordController implements Initializable {
    double maxSize;
    double minSize = 0F;
    int currentSize = 0;

    List<ReloadWordEntity> allWords;

    @FXML
    private Text englishText;

    @FXML
    private Text explanationText;

    @FXML
    private ProgressBar wordProgress;

    @FXML
    private Button showExplanationBtn;

    @FXML
    private Button nextContentBtn;

    @FXML
    private Button backBtn;

    @FXML
    private ToolBar mainBar;

    @FXML
    private AnchorPane mainBroad;


    @FXML
    private Button shuffleBtn;

    @FXML
    private Button comeBackBtn;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        compareToMin();
        reloadTheme();
    }

    private void reloadTheme() {
        FileReader fileReader = new FileReader(PathConstants.C_THEME_NAME);
        if ("LightMode".equals(fileReader.readString())) {
            lightMod();
        } else if ("DarkMode".equals(fileReader.readString())) {
            darkMod();
        }
    }

    private void lightMod() {

    }

    private void darkMod() {

        mainBroad.setBackground(new Background(new BackgroundFill(Paint
                .valueOf(ThemeColorConstants.PURPLISH_BLACK), null, null)));

        explanationText.setFill(Paint.valueOf(ThemeColorConstants.GRAY_WHITE));

        englishText.setFill(Paint.valueOf(ThemeColorConstants.GRAY_WHITE));

        showExplanationBtn.setBackground(new Background(new BackgroundFill(Paint
                .valueOf(ThemeColorConstants.MODENA_PURPLE), null, null)));
        showExplanationBtn.setTextFill(Paint.valueOf(ThemeColorConstants.GRAY_WHITE));

        nextContentBtn.setBackground(new Background(new BackgroundFill(Paint
                .valueOf(ThemeColorConstants.MODENA_PURPLE), null, null)));
        nextContentBtn.setTextFill(Paint.valueOf(ThemeColorConstants.GRAY_WHITE));

        backBtn.setBackground(new Background(new BackgroundFill(Paint
                .valueOf(ThemeColorConstants.PURPLISH_BLACK), null, null)));
        backBtn.setTextFill(Paint.valueOf(ThemeColorConstants.GRAY_WHITE));

        comeBackBtn.setBackground(new Background(new BackgroundFill(Paint
                .valueOf(ThemeColorConstants.PURPLISH_BLACK), null, null)));
        comeBackBtn.setTextFill(Paint.valueOf(ThemeColorConstants.GRAY_WHITE));

        shuffleBtn.setBackground(new Background(new BackgroundFill(Paint
                .valueOf(ThemeColorConstants.PURPLISH_BLACK), null, null)));
        shuffleBtn.setTextFill(Paint.valueOf(ThemeColorConstants.GRAY_WHITE));
        mainBar.setBackground(new Background(new BackgroundFill(Paint
                .valueOf(ThemeColorConstants.MODENA_PURPLE), null, null)));

    }

    @FXML
    void onBackClick(MouseEvent event) {
        WordBookFxApplication.showView(WordBookMainView.class);
    }

    @FXML
    void onShuffle(MouseEvent event) {
        WordService wordService = SpringUtil.getBean(WordService.class);
        allWords = wordService.getAllWordsWithTableName(LocalFileUtils.getTableName());
        maxSize = allWords.size() - 1;
        currentSize = 0;
        wordProgress.setProgress(ConvertProgressUtil.convertProgress(maxSize, minSize, currentSize));
        explanationText.setVisible(false);
        nextContentBtn.setDisable(false);
        showExplanationBtn.setDisable(false);
        Collections.shuffle(allWords);
        explanationText.setText(allWords.get(currentSize).getExplanation());
        englishText.setText(allWords.get(currentSize).getWord());
        compareToMin();
        compareToMax();
    }

    @FXML
    void onShowExplanationClick(MouseEvent event) {
        explanationText.setVisible(true);
        showExplanationBtn.setDisable(true);
    }

    @FXML
    void onNextContentClick(MouseEvent event) {
        showExplanationBtn.setDisable(false);
        compareToMax();
        compareToMin();
        try {
            explanationText.setText(allWords.get(currentSize).getExplanation());
            englishText.setText(allWords.get(currentSize).getWord());
            wordProgress.setProgress(ConvertProgressUtil
                    .convertProgress(maxSize, minSize, currentSize));
        } catch (Exception e) {
            nextContentBtn.setDisable(true);
        }
        explanationText.setVisible(false);
    }


    @FXML
    void onPrevious(MouseEvent event) {
        backToPrevious();
        compareToMin();
    }

    private void backToPrevious() {
        if (nextContentBtn.isDisable()) {
            nextContentBtn.setDisable(false);
        }
        if (showExplanationBtn.isDisable()) {
            showExplanationBtn.setDisable(false);
            explanationText.setVisible(false);
        }

        if (currentSize > minSize) {
            currentSize--;
            explanationText.setText(allWords.get(currentSize).getExplanation());
            englishText.setText(allWords.get(currentSize).getWord());
            wordProgress.setProgress(ConvertProgressUtil
                    .convertProgress(maxSize, minSize, currentSize));
        } else {
            backBtn.setDisable(true);
        }
    }

    private void compareToMin() {
        if (currentSize > minSize) {
            backBtn.setDisable(false);
        } else {
            backBtn.setDisable(true);
        }
    }

    private void compareToMax() {
        if (currentSize <= maxSize) {
            currentSize++;
        } else {
            nextContentBtn.setDisable(true);
        }
    }
}
