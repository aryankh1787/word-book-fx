package com.aryankh.wordbookfx.dao;

import com.aryankh.wordbookfx.entity.WordEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Author AryanKH
 * @Date 2023/4/20 15:48
 */
@Mapper
public interface WordDao extends BaseMapper<WordEntity> {
    /**
     * 查询SQLite中所有表
     *
     * @return String
     * @author AryanKH
     * @date 2023/5/30 9:33
     */
    @Select("SELECT name FROM sqlite_master WHERE type='table' order by name")
    List<String> allTable();


    void createTable(@Param("tableName") String tableName);

    int insertWithTableName(@Param("wordEntity") WordEntity wordEntity,
                            @Param("tableName") String tableName);

    List<WordEntity> selectAllWithTableName(
            @Param("tableName") String tableName);

    WordEntity getWordByEnglishWithTableName(@Param("word") String word,
                                             @Param("tableName") String tableName);

    List<WordEntity> getWordByChineseWithTableName(@Param("explanation") String explanation,
                                             @Param("tableName") String tablesName);


    Long getCountWithTableName(@Param("tableName") String tableName);

    int getIdByWordWithTableName(@Param("word") String word,
                                 @Param("tableName") String tableName);

    void updateExplanationWithTableName(@Param("wordId") int wordId,
                                        @Param("newExplanation") String newExplanation,
                                        @Param("tableName") String tableName);

    int getIdByWordAndExplanationWithTableName(@Param("tableName") String tableName,
                                               @Param("explanation") String explanation,
                                               @Param("word") String word);

    void deleteByIdWithTableName(@Param("wordId") int id, @Param("tableName") String tableName);

    void updateWordWithTableName(@Param("wordId") int wordId,
                                 @Param("newValue") String newValue,
                                 @Param("tableName") String tableName);
}
