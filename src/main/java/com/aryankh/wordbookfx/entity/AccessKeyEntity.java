package com.aryankh.wordbookfx.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @Author AryanKH
 * @Date 2023/4/27 19:35
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class AccessKeyEntity {
    private String accessKeyId;
    private String accessKeySecret;
}
