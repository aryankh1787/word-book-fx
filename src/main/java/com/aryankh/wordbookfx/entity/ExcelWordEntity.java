package com.aryankh.wordbookfx.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import lombok.*;

/**
 * @Author AryanKH
 * @Date 2023/8/10 13:27
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class ExcelWordEntity {
    @ColumnWidth(50)
    @ExcelProperty("Word")
    private String word;
    @ColumnWidth(50)
    @ExcelProperty("Explanation")
    private String explanation;

}
