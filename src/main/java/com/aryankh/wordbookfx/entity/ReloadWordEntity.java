package com.aryankh.wordbookfx.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @Author AryanKH
 * @Date 2023/4/20 16:11
 */

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ReloadWordEntity {
    private String word;
    private String explanation;
}
