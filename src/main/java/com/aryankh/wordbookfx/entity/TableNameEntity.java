package com.aryankh.wordbookfx.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @Author AryanKH
 * @Date 2023/5/30 13:18
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TableNameEntity {
   private String tableName;
}
