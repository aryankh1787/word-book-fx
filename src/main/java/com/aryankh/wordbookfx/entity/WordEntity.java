package com.aryankh.wordbookfx.entity;

import cn.hutool.extra.spring.SpringUtil;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @Author AryanKH
 * @Date 2023/4/20 15:27
 */
@TableName("wordbook")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class WordEntity {
    
    @TableId(value = "_id",type = IdType.AUTO)
    private Integer id;

    @TableField("word")
    private String word;

    /**
     * @author AryanKH
     * @date 2023/4/20 15:36
     * @param auto
     */
    @TableField("source_language")
    private String sourceLanguage;

    /**
     * @author AryanKH
     * @date 2023/4/20 15:36
     * @param zh-CN
     */
    @TableField("target_language")
    private String targetLanguage;

    /**
     * @author AryanKH
     * @date 2023/4/20 15:36
     * @param <null>
     */
    @TableField("phonetic")
    private String phonetic;

    @TableField("explanation")
    private String explanation;

    /**
     * @author AryanKH
     * @date 2023/4/20 15:36
     * @param <null>
     */
    @TableField("tags")
    private String tags;

    /**
     * @author AryanKH
     * @date 2023/4/20 15:36
     * @sqlite DATETIME
     * @param LocalDateTime
     */
    @TableField("created_at")
    private String createdAt;

}
