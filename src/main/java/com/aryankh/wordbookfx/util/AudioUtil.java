package com.aryankh.wordbookfx.util;

import sun.audio.AudioPlayer;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.*;
import javax.sound.sampled.*;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;

/**
 * @Author AryanKH
 * @Date 2023/7/2 16:51
 */
public class AudioUtil {
    public void downloadAudio(String audioUrl, String filePath) throws IOException {
        URL url = new URL(audioUrl);
        URLConnection connection = url.openConnection();
        InputStream inputStream = connection.getInputStream();

        BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
        FileOutputStream fileOutputStream = new FileOutputStream(filePath);

        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = bufferedInputStream.read(buffer)) != -1) {
            fileOutputStream.write(buffer, 0, bytesRead);
        }

        fileOutputStream.close();
        bufferedInputStream.close();
    }

}
