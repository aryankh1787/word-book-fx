package com.aryankh.wordbookfx.util;

/**
 * @Author AryanKH
 * @Date 2023/4/24 10:43
 */
public class CharacterUtil {
    public static boolean isChinese(String str) {
        if (str == null) {
            return false;
        }
        for (char c : str.toCharArray()) {
            if (Character.UnicodeBlock.of(c) == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS) {
                return true;
            }
        }
        return false;
    }

}
