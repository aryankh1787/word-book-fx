package com.aryankh.wordbookfx.util;

/**
 * @Author AryanKH
 * @Date 2023/4/26 9:20
 */
public class ConvertProgressUtil {
    public static double convertProgress(double maxProgress,double minProgress,double currentProgress) {
        // 计算新进度值，范围在0.0到1.0之间
        double newProgress = (currentProgress - minProgress) / (maxProgress - minProgress);

        // 确保新进度值不超出0到1之间的范围
        newProgress = Math.max(0.0, Math.min(1.0, newProgress));

        return newProgress;
    }
}
