package com.aryankh.wordbookfx.util;

import cn.hutool.core.io.file.FileReader;
import com.aryankh.wordbookfx.constant.PathConstants;

import java.util.List;

/**
 * @Author AryanKH
 * @Date 2023/6/2 7:47
 */
public class LocalFileUtils {
    public static String getTableName() {
        FileReader fileReader = new FileReader(PathConstants.TABLE_NAME_PATH, "UTF-8");
        List<String> tableNames = fileReader.readLines();
        return tableNames.get(0);
    }
}
