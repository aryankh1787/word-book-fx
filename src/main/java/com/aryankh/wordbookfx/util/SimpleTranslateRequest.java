package com.aryankh.wordbookfx.util;

import com.aliyun.alimt20181012.models.TranslateGeneralResponse;
import com.aryankh.wordbookfx.constant.TranslateRequestConstants;
import com.aryankh.wordbookfx.entity.AccessKeyEntity;
import org.junit.Test;

/**
 * @Author AryanKH
 * @Date 2023/4/28 8:58
 */
public class SimpleTranslateRequest {
    /**
     * 使用AK&SK初始化账号Client
     *
     * @param accessKeyId
     * @param accessKeySecret
     * @return Client
     * @throws Exception
     */
    private static com.aliyun.alimt20181012.Client createClient(String accessKeyId, String accessKeySecret) throws Exception {
        com.aliyun.teaopenapi.models.Config config = new com.aliyun.teaopenapi.models.Config()
                // 您的 AccessKey ID
                .setAccessKeyId(accessKeyId)
                // 您的 AccessKey Secret
                .setAccessKeySecret(accessKeySecret);
        // 访问的域名
        config.endpoint = "mt.cn-hangzhou.aliyuncs.com";
        return new com.aliyun.alimt20181012.Client(config);
    }

    public static TranslateGeneralResponse sendRequest(AccessKeyEntity accessKeyEntity,
                                                       String formatType,
                                                       String sourceLanguage,
                                                       String targetLanguage,
                                                       String sourceText, String scene
    ) throws Exception {
        com.aliyun.alimt20181012.Client client = SimpleTranslateRequest
                .createClient(accessKeyEntity.getAccessKeyId(), accessKeyEntity.getAccessKeySecret());
        com.aliyun.alimt20181012.models.TranslateGeneralRequest translateGeneralRequest =
                new com.aliyun.alimt20181012.models.TranslateGeneralRequest()
                        .setFormatType(formatType)
                        .setSourceLanguage(sourceLanguage)
                        .setTargetLanguage(targetLanguage)
                        .setSourceText(sourceText)
                        .setScene(scene);
        com.aliyun.teautil.models.RuntimeOptions runtime = new com.aliyun.teautil.models.RuntimeOptions();
        try {
            // 复制代码运行请自行打印 API 的返回值
            return client.translateGeneralWithOptions(translateGeneralRequest, runtime);
            //com.aliyun.teautil.Common.toJSONString(response)
        } catch (Exception error) {
            return null;
        }
    }
}
