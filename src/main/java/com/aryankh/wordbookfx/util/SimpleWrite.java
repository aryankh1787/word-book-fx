package com.aryankh.wordbookfx.util;

import com.alibaba.excel.EasyExcel;
import com.aryankh.wordbookfx.constant.PathConstants;
import com.aryankh.wordbookfx.entity.ExcelWordEntity;
import com.aryankh.wordbookfx.entity.ReloadWordEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author AryanKH
 * @Date 2023/8/10 13:30
 */
public class SimpleWrite {
    private List<ExcelWordEntity> data(List<ReloadWordEntity> allWords) {
        List<ExcelWordEntity> excelWordEntities = new ArrayList<>();
        allWords.forEach(word -> {
            ExcelWordEntity excelWordEntity = new ExcelWordEntity();
            excelWordEntity.setWord(word.getWord());
            excelWordEntity.setExplanation(word.getExplanation());
            excelWordEntities.add(
                    excelWordEntity
            );
        });

        return excelWordEntities;
    }

    public static void doWrite(String shortName, List<ReloadWordEntity> allWords) {
        SimpleWrite simpleWrite = new SimpleWrite();

        // 写法1 JDK8+
        // since: 3.0.0-beta1
        String fileName = PathConstants.C_PATH + shortName + System.currentTimeMillis() + ".xlsx";
        // 这里 需要指定写用哪个class去写，然后写到第一个sheet，名字为模板 然后文件流会自动关闭
        // 如果这里想使用03 则 传入excelType参数即可
        EasyExcel.write(fileName, ExcelWordEntity.class)
                .sheet("WordList")
                .doWrite(() -> {
                    // 分页查询数据
                    return simpleWrite.data(allWords);
                });
    }
}
