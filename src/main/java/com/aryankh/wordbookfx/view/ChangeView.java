package com.aryankh.wordbookfx.view;

import de.felixroske.jfxsupport.AbstractFxmlView;
import de.felixroske.jfxsupport.FXMLView;

/**
 * @Author AryanKH
 * @Date 2023/4/26 11:18
 */
@FXMLView("/fxml/change.fxml")
public class ChangeView extends AbstractFxmlView {

}
