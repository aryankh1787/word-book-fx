package com.aryankh.wordbookfx.view;

import de.felixroske.jfxsupport.AbstractFxmlView;
import de.felixroske.jfxsupport.FXMLView;

/**
 * @Author AryanKH
 * @Date 2023/4/25 19:50
 */
@FXMLView("/fxml/random.fxml")
public class RandomWordView extends AbstractFxmlView {
}
