package com.aryankh.wordbookfx.view;

import de.felixroske.jfxsupport.AbstractFxmlView;
import de.felixroske.jfxsupport.FXMLView;

/**
 * @Author AryanKH
 * @Date 2023/4/27 18:15
 */
@FXMLView("/fxml/translation.fxml")
public class TranslationView extends AbstractFxmlView {
}
