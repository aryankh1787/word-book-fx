package com.aryankh.wordbookfx.view;

import de.felixroske.jfxsupport.AbstractFxmlView;
import de.felixroske.jfxsupport.FXMLView;

/**
 * @Author AryanKH
 * @Date 2023/4/14 14:12
 */
@FXMLView("/fxml/workBookMain.fxml")
public class WordBookMainView extends AbstractFxmlView {

}
